package at.hol.baumschule;

import java.util.ArrayList;

public class Area {
	private String name;
	private int size;
	private ArrayList<Tree> trees;
	
	public Area(String name, int size) {
		super();
		this.name = name;
		this.size = size;
		this.trees = new ArrayList<>();
	}

	public ArrayList<Tree> getTrees() {
		return trees;
	}

	public void addTree(Tree tree) {
		this.trees.add(tree);
	}

	public void fertilizeAll() {
		for (Tree tree : trees) {
			tree.getUsedMethod().fertilize(tree.getMaxHeight());
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
	

}
