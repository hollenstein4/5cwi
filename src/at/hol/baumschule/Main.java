package at.hol.baumschule;

public class Main {

	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Tree t1 = new Conifer(2, 1, new SuperGrow());
		Tree t2 = new Conifer(3, 1, new TopGreen());
		Tree t3 = new LeafTree(3, 1, new TopGreen());
		
		Area a1 = new Area("area1", 3);
		a1.addTree(t1);
		a1.addTree(t2);
		a1.addTree(t3);
		
		//a1.fertilizeAll();
		
		Owner o1 = new Owner("g�nther");
		o1.addArea(a1);
		
		for (Area area : o1.getAreas()) {
			area.fertilizeAll();
		}
		

	}

}
