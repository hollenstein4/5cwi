package at.hol.baumschule;

import java.util.ArrayList;

public class Owner {
	private String name;
	private ArrayList<Area> areas;

	public Owner(String name) {
		super();
		this.name = name;
		this.areas = new ArrayList<>();
	}

	public ArrayList<Area> getAreas() {
		return areas;
	}

	public void addArea(Area area) {
		this.areas.add(area);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
}
