package at.hol.baumschule;

public class Tree {
	private int maxHeight;
	private int maxDiameter;
	private FertilizingMethod usedMethod;
	
	public Tree(int maxHeight, int maxDiameter, FertilizingMethod usedMethod) {
		super();
		this.maxHeight = maxHeight;
		this.maxDiameter = maxDiameter;
		this.usedMethod = usedMethod;
	}

	public int getMaxHeight() {
		return maxHeight;
	}

	public void setMaxHeight(int maxHeight) {
		this.maxHeight = maxHeight;
	}

	public int getMaxDiameter() {
		return maxDiameter;
	}

	public void setMaxDiameter(int maxDiameter) {
		this.maxDiameter = maxDiameter;
	}

	public FertilizingMethod getUsedMethod() {
		return usedMethod;
	}

	public void setUsedMethod(FertilizingMethod usedMethod) {
		this.usedMethod = usedMethod;
	}
	
	

}
