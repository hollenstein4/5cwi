package at.hol.engine;

public abstract class AbstractEngine implements EngineType {
	private int SerialNumber;
	
	
	public AbstractEngine(int serialNumber) {
		super();
		SerialNumber = serialNumber;
	}


	public int getSerial() {
		return this.SerialNumber;
	}
}
