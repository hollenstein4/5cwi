package at.hol.engine;

public class Car {
	private EngineType usedEngine;
	
	
	
	public Car(EngineType usedEngine) {
		super();
		this.usedEngine = usedEngine;
	}



	public void gas(int amount) {
		usedEngine.run(amount/2);
	}



	public EngineType getUsedEngine() {
		return usedEngine;
	}

	public int getSerialNumber() {
		return this.usedEngine.getSerial();
	}

	public void setUsedEngine(EngineType usedEngine) {
		this.usedEngine = usedEngine;
	}
	
	
}
