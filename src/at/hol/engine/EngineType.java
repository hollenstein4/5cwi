package at.hol.engine;

public interface EngineType {
	public void run(int amount);
	public int getSerial();
}
