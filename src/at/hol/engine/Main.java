package at.hol.engine;

public class Main {

	public static void main(String[] args) {
		Car c1 = new Car(new SuperFastGas(1234));
		c1.gas(10);
		
		//Alternative 1:
		System.out.println(c1.getSerialNumber());
		
		
		Car c2 = new Car(new TopDiesel(333));
		c2.gas(30);
		
		//Alternative 2:
		System.out.println(c2.getUsedEngine().getSerial());

	}

}
