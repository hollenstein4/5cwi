package at.hol.factory_pattern;

public interface Actor {
	public void move();
	public void render();
}
