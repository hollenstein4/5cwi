package at.hol.factory_pattern;

import java.util.ArrayList;

public class Game {
	private ArrayList<Actor> actors;
	private RandomActorFactory factory;
	
	public Game(RandomActorFactory factory) {
		this.actors = new ArrayList<>();
		this.factory = factory;
	}

	public void addActors(Actor actor) {
		this.actors.add(actor);
	}
	
	
	
	public void moveActors() {
		for (Actor actor : actors) {
			actor.move();
		}
	}
	
	public void renderActors() {
		for (Actor actor : actors) {
			actor.render();
		}
	}
}
