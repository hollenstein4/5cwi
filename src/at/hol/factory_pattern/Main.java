package at.hol.factory_pattern;

public class Main {

	public static void main(String[] args) {
		Snowflake s1 = new Snowflake();
		Actor s2 = new Snowflake();
		Snowman sm1 = new Snowman();
		Actor s3 = RandomActorFactory.createActor("at.hol.factory_pattern.Snowman");
		s3.move();
		
		Game g1 = new Game(new RandomActorFactory());
		/*
		g1.addActors(s1);
		g1.addActors(s2);
		g1.addActors(sm1);
		*/
		
		/*g1.addActors(g1.orderActor());
		g1.addActors(g1.orderActor());
		g1.addActors(g1.orderActor());
*/
		g1.renderActors();
		g1.moveActors();
	}

}
