package at.hol.factory_pattern;

public class RandomActorFactory {
	
	public static  Actor createActor(String actor) {
	
		
		 try {
	            Class c = Class.forName(actor);
	            return (Actor) c.newInstance();
	          
	         }
	         catch (Throwable e) {
	            System.err.println(e);
	         }
		return new Snowman();
	}
	
}
