package at.hol.observer_pattern;

public class Christmas implements Observable {
	private boolean isOn;
	

	public Christmas() {
		super();
		this.isOn = false;
	}
	
	

	public boolean isOn() {
		return isOn;
	}



	@Override
	public void inform() {
		this.isOn = true;
		
	}

}
