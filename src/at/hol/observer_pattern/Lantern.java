package at.hol.observer_pattern;

public class Lantern implements Observable {
	private boolean isOn;
	

	public Lantern() {
		super();
		this.isOn = false;
	}
	
	

	public boolean isOn() {
		return isOn;
	}



	@Override
	public void inform() {
		this.isOn = true;
		
	}

}