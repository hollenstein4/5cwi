package at.hol.observer_pattern;

public class Main {

	public static void main(String[] args) {
		Lantern l1 = new Lantern();
		TrafficLight tl1 = new TrafficLight();
		Christmas c1 = new Christmas();
		
		Sensor s1 = new Sensor();
		s1.addThing(l1);
		s1.addThing(tl1);
		s1.addThing(c1);


		System.out.println(l1.isOn());
		System.out.println(tl1.isOn());
		System.out.println(c1.isOn());
		
		s1.informAll();
		
		System.out.println(l1.isOn());
		System.out.println(tl1.isOn());
		System.out.println(c1.isOn());
		
	}

}
