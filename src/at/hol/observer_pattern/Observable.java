package at.hol.observer_pattern;

public interface Observable {
	public void inform();
}
