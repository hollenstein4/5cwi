package at.hol.observer_pattern;

import java.util.ArrayList;


public class Sensor {
	private ArrayList<Observable> controlledThings;
	
	public Sensor() {
		super();
		this.controlledThings = new ArrayList<>();
	}
	

	
	public void addThing(Observable thing) {
		this.controlledThings.add(thing);
	}

	
	public void informAll() {
		for (Observable observable : controlledThings) {
			observable.inform();
		}
	}

}
