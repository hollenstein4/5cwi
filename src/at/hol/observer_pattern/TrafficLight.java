package at.hol.observer_pattern;

public class TrafficLight implements Observable {
	private boolean isOn;
	

	public TrafficLight() {
		super();
		this.isOn = false;
	}
	
	

	public boolean isOn() {
		return isOn;
	}



	@Override
	public void inform() {
		this.isOn = true;
		
	}

}

