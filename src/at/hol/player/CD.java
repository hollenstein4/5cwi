package at.hol.player;

import java.util.ArrayList;

public class CD implements Media {
	private String name;
	private ArrayList<Song> songs;

	public CD(String name) {
		super();
		this.name = name;
		this.songs = new ArrayList<>();
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public ArrayList<Song> getSongs() {
		return songs;
	}

	public void addSong(Song newSong) {
		this.songs.add(newSong);
	}

	@Override
	public void loadContent(Player currentPlayer) {
		for (Song song : songs) {
			currentPlayer.addMedia(song);
		}
		System.out.println("The CD " + this.name + " has been loaded into Player " + currentPlayer);
		
	}
	
	
	
}
