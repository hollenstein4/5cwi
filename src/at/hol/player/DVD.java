package at.hol.player;

import java.util.ArrayList;

public class DVD implements Media {
	private String name;
	private ArrayList<Song> songs;

	public DVD(String name) {
		super();
		this.name = name;
		this.songs = new ArrayList<>();
	}

	public ArrayList<Song> getSongs() {
		return songs;
	}

	public void addSongs(Song newSong) {
		this.songs.add(newSong);
	}

	@Override
	public void loadContent(Player currentPlayer) {
		for (Song song : songs) {
			currentPlayer.addMedia(song);
		}
		System.out.println("The DVD " + this.name + " has been loaded into Player " + currentPlayer);
		
	}
	
	
	
}
