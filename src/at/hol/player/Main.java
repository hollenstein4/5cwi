package at.hol.player;

public class Main {

	public static void main(String[] args) {
		Player p1 = new Player("player1");
		Song s1 = new Song("song 2", 1);
		Song s2 = new Song("Despacito", 2);
		CD cd1 = new CD("�3 Hits");
		cd1.addSong(s1);
		
		cd1.loadContent(p1);
		
		p1.addMedia(s1);
		p1.addMedia(s2);
		
		p1.playAll();
	}

}
