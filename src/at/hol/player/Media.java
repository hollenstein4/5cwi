package at.hol.player;

public interface Media {
	public void loadContent(Player currentPlayer);
}
