package at.hol.player;

public interface Playable {

	public void play();
}
