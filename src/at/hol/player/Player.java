package at.hol.player;

import java.util.ArrayList;

public class Player {
	private String playerName;
	private ArrayList<Playable> media;
	
	public Player(String playerName) {
		super();
		this.playerName = playerName;
		this.media = new ArrayList<>();
	}

	

	public ArrayList<Playable> getMedia() {
		return media;
	}

	public void addMedia(Playable media) {
		this.media.add(media);
	}



	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
	
	public void playAll() {
		for (Playable playable : media) {
			playable.play();
		}
	}
	
	
}
