package at.hol.player;

public class Titel implements Playable {
	private String name;
	private int id;
	
	
	public Titel(String name, int id) {
		super();
		this.name = name;
		this.id = id;
	}

	@Override
	public void play() {
		System.out.println("Current Song running: " + name);
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

}